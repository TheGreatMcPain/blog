---
title: Reading Large Files in C, But Need 32bit Support
published: true
---

# [](#reading-large-files-but-program-needs-to-support-32bit)Reading large files, but program needs to support 32bit?

When I was writing my [OFSExtractor](https://gitlab.com/TheGreatMcPain/OFSExtractor) program I needed to do a little file IO, but since this program is designed to read video files from 3D Blurays it needed to handle files much larger than 4GB.
At the time I didn't know how to properly handle large files, so my program would only read 4GB if the program was compiled in 32bit.

Luckily, after a fair amount of research I was able to get my program working in 32bit by doing the following.

1. I Added `#define _FILE_OFFSET_BITS 64` to my c file,
2. changed all instances of `fseek` and `ftell` to `fseeko` and `ftello`,
3. and made sure all variables that store the file's offset was type `off_t`.

Hopefully this will help those who run into this issue.
